import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Navigation from './components/Navigation/Navigation';
import Home from './components/Home/Home';
import InvitationForm from './components/invitation/InvitationForm';
import MyEvents from './components/MyEvents/MyEvents';

class App extends Component {
  render () {
    return (
      <div className="App">
        <h1>Wedding Planner</h1>
        <hr />
        <BrowserRouter>
          <div>
            <Navigation />
            <Switch>
              <Route path="/" component={Home} exact />
              <Route path="/invitation" component={InvitationForm} exact />
              <Route path="/my-events" component={MyEvents} exact />
            </Switch>
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
