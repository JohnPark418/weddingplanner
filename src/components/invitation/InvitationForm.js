import React, { Component } from 'react';
import InvitationFormFields from './InvitationFormFields';
import { Formik } from 'formik';

class App extends Component {
  render () {
    return (
      <div className="container">
        <h1>Event Form</h1>
        <Formik
          initialValues={{
            email: '',
            eventTitle: '',
            eventType: '',
            hostedBy: '',
            eventDate: '',
            eventTime: '',
            locationName: '',
            street: '',
            city: '',
            state: '',
            postal: '',
          }}
          onSubmit={(values, { setSubmitting, setErrors }) => {
            console.log(values);
          }}
          validate={values => {
            const errors = {};
            if (!values.email) {
              errors.email = 'Required';
            } else if (
              !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
            ) {
              errors.email = 'Invalid email address';
            }

            if (!values.eventTitle)
              errors.eventTitle = 'Event title is required';
            if (!values.eventType || values.eventType === '- Select -')
              errors.eventType = 'Event type is required';
            if (!values.hostedBy) errors.hostedBy = 'Host name is required';

            if (!values.eventDate) {
              errors.eventDate = 'Please select date';
            } else if (Date.parse(values.eventDate) < new Date()) {
              values.eventDate = '';
              errors.eventDate = 'Please select another date';
            }
            if (!values.eventTime) errors.eventTime = 'Please select time';

            if (!values.locationName)
              errors.locationName = 'Location name is required';
            if (!values.street) errors.street = 'Street is required';
            if (!values.city) errors.city = 'City is required';
            if (!values.state) errors.state = 'State is required';

            if (!values.postal) {
              errors.postal = 'Postal code is required';
            } else if (values.postal.replace(/\s/g, '').length !== 6) {
              errors.postal = 'Please put the correct postal code';
            }
            return errors;
          }}
          render={({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
          }) => (
            <InvitationFormFields
              values={values}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
              handleSubmit={handleSubmit}
              isSubmitting={isSubmitting}
            />
          )}
        />
      </div>
    );
  }
}

export default App;
