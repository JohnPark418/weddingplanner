import React from 'react';
import './InvitationFormFields.css';
import PropTypes from 'prop-types';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

const InvitationFormFields = ({
  values,
  errors,
  touched,
  handleChange,
  handleBlur,
  handleSubmit,
  isSubmitting,
}) => (
  <Form onSubmit={handleSubmit} id="invitation-form">
    <FormGroup>
      <Label for="email">Email</Label>
      <Input
        type="email"
        name="email"
        onChange={handleChange}
        onBlur={handleBlur}
        value={values.email}
      />
      {touched.email &&
        errors.email && <div className="error-message">{errors.email}</div>}
    </FormGroup>
    <FormGroup>
      <Label for="eventTitle">Event Title</Label>
      <Input
        type="text"
        name="eventTitle"
        onChange={handleChange}
        onBlur={handleBlur}
        value={values.eventTitle}
      />
      {touched.eventTitle &&
        errors.eventTitle && (
          <div className="error-message">{errors.eventTitle}</div>
        )}
    </FormGroup>
    <FormGroup>
      <Label for="eventType">Event Type</Label>
      <Input
        type="select"
        name="eventType"
        onChange={handleChange}
        onBlur={handleBlur}
        value={values.eventType}
      >
        <option>- Select -</option>
        <option>Wedding</option>
        <option>Birthday Party</option>
        <option>Parade</option>
        <option>Hackathon</option>
        <option>Street Fight</option>
      </Input>
      {touched.eventType &&
        errors.eventType && (
          <div className="error-message">{errors.eventType}</div>
        )}
    </FormGroup>
    <FormGroup>
      <Label for="hostedBy">Hosted By</Label>
      <Input
        type="text"
        name="hostedBy"
        onChange={handleChange}
        onBlur={handleBlur}
        value={values.hostedBy}
      />
      {touched.hostedBy &&
        errors.hostedBy && (
          <div className="error-message">{errors.hostedBy}</div>
        )}
    </FormGroup>
    <FormGroup>
      <Label for="eventDate">Event Date</Label>
      <Input
        type="date"
        name="eventDate"
        onChange={handleChange}
        onBlur={handleBlur}
        value={values.eventDate}
      />
      <Input
        type="time"
        name="eventTime"
        onChange={handleChange}
        onBlur={handleBlur}
        value={values.eventTime}
      />
      {touched.eventDate &&
        errors.eventDate && (
          <div className="error-message">{errors.eventDate}</div>
        )}
      {touched.eventTime &&
        errors.eventTime && (
          <div className="error-message">{errors.eventTime}</div>
        )}
    </FormGroup>
    <FormGroup>
      <Label for="locationName">Location Name</Label>
      <Input
        type="text"
        name="locationName"
        onChange={handleChange}
        onBlur={handleBlur}
        value={values.locationName}
      />
      {touched.locationName &&
        errors.locationName && (
          <div className="error-message">{errors.locationName}</div>
        )}
    </FormGroup>
    <FormGroup>
      <Label for="street">Street</Label>
      <Input
        type="text"
        name="street"
        onChange={handleChange}
        onBlur={handleBlur}
        value={values.street}
      />
      {touched.street &&
        errors.street && <div className="error-message">{errors.street}</div>}
    </FormGroup>
    <FormGroup>
      <Label for="city">City</Label>
      <Input
        type="text"
        name="city"
        onChange={handleChange}
        onBlur={handleBlur}
        value={values.city}
      />
      {touched.city &&
        errors.city && <div className="error-message">{errors.city}</div>}
    </FormGroup>
    <FormGroup>
      <Label for="state">State</Label>
      <Input
        type="text"
        name="state"
        onChange={handleChange}
        onBlur={handleBlur}
        value={values.state}
      />
      {touched.state &&
        errors.state && <div className="error-message">{errors.state}</div>}
    </FormGroup>
    <FormGroup>
      <Label for="postal">postal</Label>
      <Input
        type="text"
        name="postal"
        onChange={handleChange}
        onBlur={handleBlur}
        value={values.postal.toUpperCase()}
      />
      {touched.postal &&
        errors.postal && <div className="error-message">{errors.postal}</div>}
    </FormGroup>
    <Button type="submit" color="primary">
      SUBMIT
    </Button>{' '}
  </Form>
);

InvitationFormFields.propTypes = {
  values: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
};

export default InvitationFormFields;
