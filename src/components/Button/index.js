import React from 'react';
import { SpinnerContainer, Spinner } from './style';

export const Button = () => {
  return (
    <SpinnerContainer>
      <Spinner size="16" />
    </SpinnerContainer>
  );
};
