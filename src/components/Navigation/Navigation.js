import React from 'react';
import { NavLink } from 'react-router-dom';

const Navigation = () => {
  return (
    <div>
      <NavLink to="/">Home</NavLink> |
      <NavLink to="/invitation">Create a New Event</NavLink> |
      <NavLink to="/my-events">My Events</NavLink>
      <hr />
    </div>
  );
};

export default Navigation;
