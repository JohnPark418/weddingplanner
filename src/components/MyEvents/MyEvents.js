import React, { Component } from 'react';

class MyEvents extends Component {
  state = {
    guestList: [],
    giftRegistry: [],
    showGuests: false,
    showGiftRegistry: false,
  };

  renderGuestList = () => {
    const { showGuests } = this.state;

    this.setState({
      guestList: ['Girl1', 'Girl2', 'Girl3', 'Suzy', 'IU'],
      showGuests: !showGuests,
    });
  };

  renderGiftRegistry = () => {
    const { showGiftRegistry } = this.state;

    this.setState({
      giftRegistry: ['lambourghini', 'ferrari', 'yacht'],
      showGiftRegistry: !showGiftRegistry,
    });
  };

  render () {
    const guestList = this.state.guestList.map((g, index) => {
      return <div key={index}>{g}</div>;
    });

    const giftRegistry = this.state.giftRegistry.map((g, index) => {
      return <div key={index}>{g}</div>;
    });

    return (
      <div>
        <h2>My Events</h2>

        <h4>Wedding</h4>
        <button onClick={this.renderGuestList}>Guests</button>
        {this.state.showGuests ? guestList : null}
        <button onClick={this.renderGiftRegistry}>Gift Registry</button>
        {this.state.showGiftRegistry ? giftRegistry : null}

        <h4>Birthday</h4>
        <button onClick={this.renderGuestList}>Guests</button>
        <button onClick={this.renderGiftRegistry}>Gift Registry</button>

        <h4>Graduation</h4>
        <button onClick={this.renderGuestList}>Guests</button>
        <button onClick={this.renderGiftRegistry}>Gift Registry</button>
      </div>
    );
  }
}

export default MyEvents;
