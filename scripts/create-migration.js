const shell = require('shelljs');

if (process.argv.length > 3) {
  throw new Error('');
}

shell.exec(
  `db-migrate create ${
    process.argv[2]
  } --env dev --migrations-dir ./server/migrations --sql-file`
);
