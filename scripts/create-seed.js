const shell = require('shelljs');

if (process.argv.length > 3) {
  throw new Error('');
}

shell.exec(`knex seed:make ${process.argv[2]}`);
