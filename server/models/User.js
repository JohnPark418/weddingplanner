const { Model } = require('objection');

class User extends Model {
  static get tableName () {
    return 'user';
  }

  static get relationMappings () {
    return {
      // children: {
      //   relation: Model.HasManyRelation,
      //   modelClass: User,
      //   join: {
      //     from: 'persons.id',
      //     to: 'persons.parentId'
      //   }
      // }
    };
  }
}

module.exports = User;
