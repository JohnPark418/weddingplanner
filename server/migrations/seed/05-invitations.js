const faker = require('faker');

exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries
  return knex('invitation')
    .del()
    .then(function () {
      // Inserts seed entries
      return knex('invitation').insert([
        { guest_id: 1, num_guests: 0, is_coming: false },
      ]);
    });
};
