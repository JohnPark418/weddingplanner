exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries
  return knex('guest')
    .del()
    .then(function () {
      // Inserts seed entries
      return knex('guest').insert([
        {
          host_id: 1,
          first_name: 'Steve',
          last_name: 'Kim',
          phone_number: '778-888-1111',
          email: 'dkim.steve@gmail.com',
        },
      ]);
    });
};
