const faker = require('faker');

const createEventTemplate = () => {
  return {};
};

exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries
  return knex('event_template')
    .del()
    .then(function () {
      // Inserts seed entries

      // let numRows = 50;
      // const templates = []
      // for (let i = 0; i < numRows; i++) {
      //   templates.push()
      // }

      return knex('event_template').insert([
        {
          id: 1,
          html: '<div>Hello</div>',
          css: null,
        },
      ]);
    });
};
