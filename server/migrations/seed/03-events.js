const faker = require('faker');
const moment = require('moment');

exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries
  return knex('event')
    .del()
    .then(function () {
      // Inserts seed entries
      return knex('event').insert([
        {
          user_id: 1,
          event_template_id: 1,
          event_name: "Dohoon's Wedding",
          location: 'Vancouver',
          event_datetime: moment().add(1, 'week'),
        },
      ]);
    });
};
