CREATE TABLE public.Invitation
(
    id serial PRIMARY KEY NOT NULL,
    guest_id int NOT NULL,
    num_guests int,
    is_coming boolean NOT NULL,
    CONSTRAINT invitation_guest_id_fk FOREIGN KEY (guest_id) REFERENCES guest (id)
);
