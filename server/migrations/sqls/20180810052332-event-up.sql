CREATE TABLE public.Event
(
    id serial PRIMARY KEY NOT NULL,
    user_id int NOT NULL,
    event_template_id int NOT NULL,
    event_name varchar(1024) NOT NULL,
    location varchar(1024) NOT NULL,
    event_datetime TIMESTAMP with time zone,
    CONSTRAINT Event_user_id_fk FOREIGN KEY (user_id) REFERENCES "user" (id),
    CONSTRAINT Event_event_template_id_fk FOREIGN KEY (event_template_id) REFERENCES event_template (id)
)




