CREATE TABLE public.Guest
(
    id serial PRIMARY KEY NOT NULL,
    host_id int NOT NULL,
    first_name varchar(100) NOT NULL,
    last_name varchar(100) NOT NULL,
    phone_number varchar(50),
    email varchar(254),
    CONSTRAINT Guest_user_id_fk FOREIGN KEY (host_id) REFERENCES "user" (id)
)
