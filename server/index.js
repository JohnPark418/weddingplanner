require('dotenv').config();
const debug = require('debug')('Server');
const path = require('path');
const express = require('express');
const app = express();

debug('DATABASE URL', process.env.DATABASE_URL);
const db = require('./db');

const expressHandlebars = require('express-handlebars');
const templatesDirPath = path.resolve(__dirname, 'templates');
const hbs = expressHandlebars.create({
  defaultLayout: 'main',
  layoutsDir: templatesDirPath,
  partialsDir: templatesDirPath,
});

app.engine('handlebars', hbs.engine);
app.set('views', path.resolve(__dirname, 'templates'));
app.set('view engine', 'handlebars');

const data = {
  johnpark418: {
    johnparkWedding: {
      guests: {
        stevekim: false,
        dohoonkim: true,
        wonpark: false,
      },
    },
  },
};

// TODO: Authenticate user
app.get('/:userId/:eventId/:templateId/:guestId/:view', async (req, res) => {
  console.log('Req.params ', req.params);
  const { userId, templateId, eventId, guestId, view } = req.params;

  // Retrieve user's guest list for given event
  // const event = data[userId][eventId];

  // Check if event and guest exist
  // if (event && guestId in event) {
  //   res.render(templateId, {});
  // } else {
  //   // TODO: Redirect...
  // }

  // const { html } = await db('event_template').where("id", templateId).select("*")
  const { html } = await db('event_template')
    .where('id', templateId)
    .first();

  // console.log("content ", content)
  console.log('Template 1  html ', html);

  res.render('example', {
    pageCss: `
      h1 {color:palevioletred;}
      p {color:blue;}
    `,
    htmlContent: html,
  });
});

const UserModel = require('./models/User');

app.listen(process.env.PORT, async () => {
  debug('Server started at', process.env.PORT);

  const knexUsers = await db('user');
  console.log('Knex ', knexUsers);

  // const users = await UserModel.query();
  // console.log('Users ', users);
});
