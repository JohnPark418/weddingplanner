const { Model } = require('objection');
const knex = require('knex');

const DEFAULT_CONFIG = {
  client: 'pg',
  useNullAsDefault: true,
  connection: process.env.DATABASE_URL,
};

const dbInstance = knex({ ...DEFAULT_CONFIG });

// TODO: add logger

Model.knex(dbInstance);

module.exports = dbInstance;
