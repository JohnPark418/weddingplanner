// Update with your config settings.
require('dotenv').config();

console.log('process.env.DATABASE_URL ', process.env.DATABASE_URL);

module.exports = {
  development: {
    client: 'pg',
    useNullAsDefault: true,
    connection: process.env.DATABASE_URL,
    seeds: {
      directory: __dirname + '/server/migrations/seed/',
    },
  },

  production: {
    client: 'pg',
    connection: {
      database: 'wedding_planner',
      user: 'admin',
      password: 'aaaa',
    },
    pool: {
      min: 2,
      max: 10,
    },
    // migrations: {
    //   directory: './server/migrations',
    //   tableName: 'knex_migrations',
    // },
  },
};
