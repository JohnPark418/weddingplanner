# DB Schema

## Tables
1. User
  - id
  - email
  - password

2. Event
  - id
  - user_id (FK)
  - event_template_id (FK)
  - event_name
  - location
  - event_datetime
  
3. EventTemplate
  - id
  - html (string)
  - css (string)

4. Invitation
  - id
  - guest_id (FK)
  - is_coming
  - num_guests

5. Guest
  - id
  - first_name
  - last_name
  - phone_number
  - email
  - 